// usb.findByIds(0x8087, 0x0A2B) @ usb.js:66
// this is for Lenovo thinkpad t570

// for manufacturer advertisement see https://github.com/sandeepmistry/node-bleacon/blob/master/lib/bleacon.js
// accuracy is on the 95th line

require('events').EventEmitter.prototype._maxListeners = 100;
const rp = require('request-promise-native');
const fs = require('fs');
const path = require('path');
const noble = require('noble');
const os = require('os');

const targetDir = 'generated';
const initDir = path.isAbsolute(targetDir) ? path.sep : '';

const EXPECTED_MANUFACTURER_DATA_LENGTH = 25;
const APPLE_COMPANY_IDENTIFIER = 0x004c; // https://www.bluetooth.org/en-us/specification/assigned-numbers/company-identifiers
const IBEACON_TYPE = 0x02;
const EXPECTED_IBEACON_DATA_LENGTH = 0x15;

const kontaktUuid = 'f7826da6-4fa2-4e98-8024-bc5b71e0893e';

/*`${targetDir}`.split(path.sep).reduce((parentDir, childDir) => {
    const curDir = path.resolve(parentDir, childDir);

    if (!fs.existsSync(curDir)) {
        fs.mkdirSync(curDir);
    }

    return curDir;
}, initDir);*/

noble.on('stateChange', function (state) {
    console.log('State changed', state);

    if (state === 'poweredOn') {
        noble.startScanning([], true, (err, filterDuplicates) => {
            console.log(err);
            console.log(filterDuplicates);
        });
    } else {
        noble.stopScanning();
    }
});

noble.on('discover', p => {
    // console.dir(p.uuid);

    if (p.advertisement.localName == 'Kontakt') {
        console.log('Connecting to:', p.uuid);
        // console.dir(p.advertisement);

        let pToSave = {...p};

        if(pToSave.advertisement && pToSave.advertisement.manufacturerData) {
            console.log('Parsing advertisement buffer...');

            pToSave.gateway = {
                address: os.networkInterfaces().wlan0
            };

            let uuid = pToSave.advertisement.manufacturerData.slice(4, 20).toString('hex');
            let major = pToSave.advertisement.manufacturerData.readUInt16BE(20);
            let minor = pToSave.advertisement.manufacturerData.readUInt16BE(22);
            let measuredPower = pToSave.advertisement.manufacturerData.readInt8(24);

            if (pToSave.rssi == 0) {
                return -1.0;
            }

            let ratio = pToSave.rssi * 1.0 / measuredPower;
            let distance;
            if (ratio < 1.0) {
                distance = Math.pow(ratio, 10);
            } else {
                distance = (0.89976) * Math.pow(ratio, 7.7095) + 0.111;
            }

            console.log('Data for', pToSave.uuid);
            // console.log('Uuid:\t', uuid);
            // console.log('Major:\t', major);
            // console.log('Minor:\t', minor);
            console.log('tx:\t', pToSave.advertisement.txPowerLevel);
            console.log('MeasurePower:\t', measuredPower);
            console.log('Distance:\t', distance);
            console.log('Accuracy:\t', Math.pow(12.0, 1.5 * ((pToSave.rssi / measuredPower) - 1)));

            pToSave.advertisement.uuid = uuid;
            pToSave.advertisement.major = major;
            pToSave.advertisement.minor = minor;
            pToSave.advertisement.measuredPower = measuredPower;
            pToSave.advertisement.accuracy = Math.pow(12.0, 1.5 * ((pToSave.rssi / measuredPower) - 1));
            pToSave.advertisement.distance = distance;

            if (pToSave.advertisement.accuracy < 0) {
                pToSave.advertisement.proximity = 'unknown';
            } else if (pToSave.advertisement.accuracy < 0.5) {
                pToSave.advertisement.proximity = 'immediate';
            } else if (pToSave.advertisement.accuracy < 4.0) {
                pToSave.advertisement.proximity = 'near';
            } else {
                pToSave.advertisement.proximity = 'far';
            }

            delete pToSave.advertisement.manufacturerData;

            console.log('MeasurePower:\t', pToSave.advertisement.proximity);

            rp.post({
                method: 'POST',
                uri: 'http://192.168.0.236:3000/api/beacons/',
                body: {
                    gateway: pToSave.gateway,
                    advertisement: pToSave.advertisement,
                    uuid: pToSave.uuid,
                    rssi: pToSave.rssi,
                },
                json: true // Automatically stringifies the body to JSON
            }).then(response => {
                console.log('Sent to API');
            }, err => {
                console.log(err);
            });
        }

        /*if (p.advertisement && p.advertisement.manufacturerData && p.advertisement.manufacturerData.data &&
                EXPECTED_MANUFACTURER_DATA_LENGTH <= p.advertisement.manufacturerData.length &&
                APPLE_COMPANY_IDENTIFIER === p.advertisement.manufacturerData.readUInt16LE(0) &&
                IBEACON_TYPE === p.advertisement.manufacturerData.readUInt8(2) &&
                EXPECTED_IBEACON_DATA_LENGTH === p.advertisement.manufacturerData.readUInt8(3)) {*/

        /*p.connect(err => {
            if(err) console.log(err);

            console.log('Connected to peripheral:', p.uuid);
            // console.dir(p.advertisement);

            let pToSave = {...p};

            pToSave.gateway = {
                address: os.networkInterfaces().wlan0
            };

            delete pToSave._noble;
            delete pToSave._events;
            delete pToSave._eventsCount;

            // console.log('Advertisement?\t', !!pToSave.advertisement);
            // console.log('Manufacturer? \t', pToSave.advertisement && pToSave.advertisement.manufacturerData);
            // console.log('Manuf data?\t', pToSave.advertisement && pToSave.advertisement.manufacturerData && pToSave.advertisement.manufacturerData.data);

            if(pToSave.advertisement && pToSave.advertisement.manufacturerData) {
                console.log('Parsing advertisement buffer...');

                let uuid = pToSave.advertisement.manufacturerData.slice(4, 20).toString('hex');
                let major = pToSave.advertisement.manufacturerData.readUInt16BE(20);
                let minor = pToSave.advertisement.manufacturerData.readUInt16BE(22);
                let measuredPower = pToSave.advertisement.manufacturerData.readInt8(24);

                console.log('Data for', pToSave.uuid);
                // console.log('Uuid:\t', uuid);
                // console.log('Major:\t', major);
                // console.log('Minor:\t', minor);
                // console.log('MeasurePower:\t', measuredPower);
                console.log('Accuracy:\t', Math.pow(12.0, 1.5 * ((pToSave.rssi / measuredPower) - 1)));

                pToSave.advertisement.uuid = uuid;
                pToSave.advertisement.major = major;
                pToSave.advertisement.minor = minor;
                pToSave.advertisement.measuredPower = measuredPower;
                pToSave.advertisement.accuracy = Math.pow(12.0, 1.5 * ((pToSave.rssi / measuredPower) - 1));

                if (pToSave.advertisement.accuracy < 0) {
                    pToSave.advertisement.proximity = 'unknown';
                } else if (pToSave.advertisement.accuracy < 0.5) {
                    pToSave.advertisement.proximity = 'immediate';
                } else if (pToSave.advertisement.accuracy < 4.0) {
                    pToSave.advertisement.proximity = 'near';
                } else {
                    pToSave.advertisement.proximity = 'far';
                }

                delete pToSave.advertisement.manufacturerData;

                console.log('MeasurePower:\t', pToSave.advertisement.proximity);

                rp.post({
                    method: 'POST',
                    uri: 'http://192.168.0.236:3000/api/beacons/',
                    body: {
                        gateway: pToSave.gateway,
                        advertisement: pToSave.advertisement,
                        uuid: pToSave.uuid
                    },
                    json: true // Automatically stringifies the body to JSON
                }).then(response => {
                    console.log('Sent to API');
                }, err => {
                    console.log(err);
                });
            }

            /!*p.discoverServices(null, (error, services) => {
                services.forEach(s => {
                    if (s.name && s.name.split(' ').length > 0) {
                        let serviceName = camelize(s.name);
                        pToSave[serviceName] = {
                            name: s.name,
                            type: s.type,
                            uuid: s.uuid
                        };

                        s.discoverCharacteristics(null, (error, characteristics) => {
                            characteristics.forEach((c, idx) => {
                                c.read((error, data) => {
                                    if (c.name && c.name.split(' ').length > 0) {
                                        let charName = camelize(c.name);
                                        pToSave[serviceName][charName] = {
                                            dataUtf8: data.toString('utf8'),
                                            rawData: data,
                                            name: c.name,
                                            type: c.type,
                                            uuid: c.uuid,
                                            properties: c.properties
                                        };

                                        if (idx == characteristics.length - 1) {
                                            // rp.post('http://192.168.0.236:3000/api/beacons/', pToSave);
                                            rp.post({
                                                method: 'POST',
                                                uri: 'http://192.168.0.236:3000/api/beacons/',
                                                body: pToSave,
                                                json: true // Automatically stringifies the body to JSON
                                            });

                                            fs.writeFile(`${targetDir}${path.sep}${pToSave.uuid}.json`, JSON.stringify(pToSave, null, 4), () => {
                                                console.log(`${pToSave.uuid} saved!`);
                                                p.disconnect();
                                                console.log('Disconnected peripheral:', p.uuid);
                                            });

                                            try {
                                                noble.startScanning();
                                            } catch(e) {
                                                console.log(`Can't scan because of error`);
                                                console.dir(e);
                                            }
                                        }
                                    }
                                });
                            });
                        });
                    }
                });
            });*!/
        });*/
    }
});

function camelize(str) {
    let name = str.split(' ');
    name[0] = name[0].toLowerCase();

    return name.join('');
}